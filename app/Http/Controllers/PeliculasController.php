<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peliculas;

class PeliculasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'nombre' => 'required|integer',
            'genero' => 'required|string',
            'genero' => 'required|string',
        ]);

        Comentarios::create([
            'comentarios' => $request->comentario,
            'idPeliculas' => $request->pelicula,
        ]);

        return response()->json([
            'message' => 'Successfully created coment!'
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showForById($id)
    {
        //
        return Peliculas::where('id', $id)->get();
    }

    public function showAllMovies()
    {
        return Peliculas::select('nombre','genero', 'precio', 'anioLanzamiento', 'descripcion')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
